/**
 * @format
 */

import App from './App';
import {Navigation} from 'react-native-navigation';
import App1 from './App1';

// register screen
Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
Navigation.registerComponent(
  `navigation.playground.WelcomeScreen1`,
  () => App1,
);

// set the app launcher
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'navigation.playground.WelcomeScreen',
            },
          },
        ],
      },
    },
  });
});
